# Smart Home Bridge #

Smart Home Bridge is the ESP8266 based home appliances controller. It uses local WiFi network and 
Alexa as a personal assistance to control home appliances. It uses FauxmoESP library.


### Dependancies ###

* [FauxmoESP](https://bitbucket.org/xoseperez/fauxmoesp/src/master/)
* [ESPAsyncTCP](https://github.com/me-no-dev/ESPAsyncTCP)
* [AutoConnect](https://github.com/Hieromon/AutoConnect)

### Circuit Diagram ###
![alt text](docs/ConnectionDiagram.jpg?raw=true)